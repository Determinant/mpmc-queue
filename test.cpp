#include <thread>
#include <unistd.h>
#include "queue.hpp"


template<typename Q>
void test_queue(Q &q) {
    std::vector<std::thread> threads;
    int n = 16;
    int m = 1; //100000;
    int l = 100000;
    for (int i = 0; i < n; i++)
    {
        threads.push_back(std::thread([m, n, l, i, &q]() {
            int x = i;
            for (int j = 0; j < m; j++)
            {
                //while (!q.enqueue(x));// usleep(10 * rand() / (double)(RAND_MAX));
                while (!q.enqueue(x)) usleep(10 * rand() / (double)(RAND_MAX));
                x += n;
            }
            int xx;
            for (int k = 0; k < l; k++)
            {
                //while (!q.try_dequeue(xx));
                //while (!q.enqueue(xx));
                //while (!q.try_dequeue(xx));
                //while (!q.enqueue(xx));

                while (!q.try_dequeue(xx)) usleep(10 * rand() / (double)(RAND_MAX));
                while (!q.enqueue(xx)) usleep(10 * rand() / (double)(RAND_MAX));
                while (!q.try_dequeue(xx)) usleep(1000 * rand() / (double)(RAND_MAX));
                while (!q.enqueue(xx)) usleep(1000 * rand() / (double)(RAND_MAX));
            }
        }));
    }
    for (auto &t: threads)
        t.join();
    int x;
    while (q.try_dequeue(x))
        printf("%d\n", x);
}

using freelist_t = determinant::FreeList;

void test_freelist(freelist_t &s) {
    std::vector<std::thread> threads;
    int n = 16;
    int m = 1; //1000;
    int l = 100000;
    auto nodes = new freelist_t::Node[n * m];
    for (int i = 0; i < n; i++)
    {
        threads.push_back(std::thread([m, n, l, i, &s, nodes]() {
            int x = i;
            for (int j = 0; j < m; j++)
            {
                while (!s.push(nodes + x)) usleep(10 * rand() / (double)(RAND_MAX));
                x += n;
            }
            freelist_t::Node *ptr;
            for (int k = 0; k < l; k++)
            {
                while (!s.pop(ptr)) usleep(10 * rand() / (double)(RAND_MAX));
                while (!s.push(ptr)) usleep(10 * rand() / (double)(RAND_MAX));
                while (!s.pop(ptr)) usleep(10 * rand() / (double)(RAND_MAX));
                while (!s.push(ptr)) usleep(10 * rand() / (double)(RAND_MAX));
            }
        }));
    }
    for (auto &t: threads) t.join();
    freelist_t::Node *ptr;
    while (s.pop(ptr))
        printf("%lx\n", (uintptr_t)ptr);

    delete [] nodes;
}

int main() {
    //freelist_t s;
    //test_freelist(s);
    determinant::MPMCQueue<int> q(10);
    test_queue(q);
    return 0;
}
